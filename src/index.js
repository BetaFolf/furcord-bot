/*
* Author: Beta Folf
* Project: Furcord/Bot
* File: src/index.js
*/

// Imports
require("dotenv").config();

const { Bot } = require("discordbull");
const path = require("path");

new Bot({
    TOKEN: process.env.TOKEN,
    OWNER_IDS: ["412388163001122838"],
    EMBED_COLOR: "0x007289DA",
    COGS_LOCATION: path.join(__dirname, "./Cogs")
});