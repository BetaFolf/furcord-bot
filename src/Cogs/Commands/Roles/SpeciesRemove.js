/*
 * Author: Beta Folf
 * Project: Furcord/Bot
 * File: src/Cogs/Commands/Moderation/SpeciesRemove.js
 */

// Imports
const { Command } = require('discordbull')
const rolesJson = require('../../../Files/SpeciesRoles.json')

// Command
class SpeciesCommand extends Command {
  constructor() {
    super({
      NAME: 'speciesremove',
      USAGE: 'speciesremove',
      NICKS: ['iamnot'],
      ARGS: [
        {
          NAME: 'species',
          REQUIRED: true
        }
      ],
      GUILD_ONLY: true,
      RUN: async (context, args) => {
        let role = args[0]

        let roleExists = false
        let roleToRemove = null

        for (let x = 0; x < rolesJson.length; x++) {
          if (rolesJson[x]['name'].toUpperCase() === role.toUpperCase()) {
            roleExists = true
            roleToRemove = rolesJson[x]
          }
        }

        if (roleExists === false) {
          return await context.channel.send('That role does not exist!')
        } else {
            if (context.member.roles.has(roleToRemove['id'])) {
                await context.member.removeRole(roleToRemove['id'])
                await context.react("✅");
            } else {
                return await context.channel.send("You don't have that role!");
            }
        }
      }
    })
  }
}

exports.default = SpeciesCommand
