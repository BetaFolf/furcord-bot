/*
 * Author: Beta Folf
 * Project: Furcord/Bot
 * File: src/Cogs/Commands/Moderation/SpeciesAdd.js
 */

// Imports
const { Command } = require('discordbull')
const rolesJson = require('../../../Files/SpeciesRoles.json')

// Command
class SpeciesCommand extends Command {
  constructor() {
    super({
      NAME: 'speciesadd',
      USAGE: 'speciesadd',
      NICKS: ['iam'],
      ARGS: [
        {
          NAME: 'species',
          REQUIRED: true
        }
      ],
      GUILD_ONLY: true,
      RUN: async (context, args) => {
        let role = args[0]

        let roleExists = false
        let roleToAdd = null

        for (let x = 0; x < rolesJson.length; x++) {
          if (rolesJson[x]['name'].toUpperCase() === role.toUpperCase()) {
            roleExists = true
            roleToAdd = rolesJson[x]
          }
        }

        if (roleExists === false) {
          return await context.channel.send('That role does not exist!')
        } else {
          await context.member.addRole(roleToAdd['id'].toString());
          await context.react("✅");
        }
      }
    })
  }
}

exports.default = SpeciesCommand
