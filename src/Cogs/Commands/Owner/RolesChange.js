/*
* Author: Beta Folf
* Project: Furcord/Bot
* File: src/Cogs/Commands/Owner/RolesChanges.js
*/

// Imports
const { Bot, Command } = require("discordbull");

// Command
class RolesChangeCommand extends Command {
    constructor() {
        super({
            NAME: "roleschange",
            USAGE: "roleschange",
            OWNER_ONLY: true,
            RUN: (context) => {
                context.channel.send("Giving everyone the verified role. This is a one time command to catch everyone up with the new verification system...");

                    try {
                        context.guild.members.forEach(async m => {
                            if (m.roles.has("571730226418286613") === false) {
                                m.addRole("571730226418286613").then(() => {
                                    console.log(`Added verified role to '${m.user.username}#${m.user.discriminator}'.`)
                                }).catch(error => {
                                    throw BreakException;
                                })
                            }    
                        })
                    } catch(e) {
                        console.log("Done.")
                    }

                console.log("Done");
                context.channel.send("Done!");
            }
        });
    }
}

exports.default = RolesChangeCommand;