/*
* Author: Beta Folf
* Project: Furcord/Bot
* File: src/Cogs/Commands/Owner/Say.js
*/

// Imports
const { Command } = require("discordbull");

// Command
class SayCommand extends Command {
    constructor() {
        super({
            NAME: "say",
            USAGE: "say",
            OWNER_ONLY: true,
            ARGS: [
                {
                    NAME: "text",
                    REQUIRED: true
                }
            ],
            RUN: (async(context, args) => {
                let text = args.join(" ")
                
                context.delete().then(async msg => {
                    await context.channel.send(text);
                }).catch(error => {
                    console.log("Say command error: " + error)
                })
            })
        });
    }
}

exports.default = SayCommand;