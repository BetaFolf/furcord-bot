/*
* Author: Beta Folf
* Project: Furcord/Bot
* File: src/Cogs/Commands/Owner/Reload.js
*/

// Imports
const { Command } = require("discordbull");
const fs = require("fs");
const path = require("path");

// Command
class ReloadCommand extends Command {
    constructor() {
        super({
            NAME: "reload",
            USAGE: "reload",
            OWNER_ONLY: true,
            RUN: (async (context) => {
                let success = 0;
                let failure = 0;

                let location = path.join(__dirname, "../../../Cogs");
                const loadCogs = (cogsLocation) => {
                    fs.readdirSync(cogsLocation).forEach(file => {
                        let fileLocation = path.join(cogsLocation, file);
                        if (fs.statSync(fileLocation).isDirectory()) {
                            loadCogs(fileLocation);
                        } else {
                            try {
                                delete require.cache[require.resolve(fileLocation)];
                                let f = require(fileLocation).default;
                                new f();
                                console.log(`Reloaded cog '${file}'.`);
                                success = success + 1;
                            } catch(e) {
                                console.log(`Could not reload cog '${file}'. \nError: '${e}'`);
                                failure = failure + 1;
                            }        
                        }
                    });
                }

                loadCogs(location);

                await context.channel.send(`Reloaded cogs! ${success} success. ${failure} failed.`);
            }
        )});
    }
}

exports.default = ReloadCommand;