/*
* Author: Beta Folf
* Project: Furcord/Bot
* File: src/Cogs/Owner/FakeUser.js
*/

// Imports
const { WebhookClient } = require("discord.js");
const { Command, Bot } = require("discordbull");

// Command
class FakeUserCommand extends Command {
    constructor() {
        super({
            NAME: "fakeuser",
            USAGE: "fakeuser",
            args: [
                {
                    NAME: "user",
                    required: true
                },
                {
                    NAME: "message",
                    required: true
                }
            ],
            RUN: (async (context, args) => {
                let user = args[0]
                args.shift()
                let textToSay = args.join(" ");

                let userInformation = await Bot.DiscordClient.fetchUser(user);
                let webhook = await context.channel.createWebhook(userInformation.username, userInformation.displayAvatarURL);

                await context.delete();
                await webhook.send(textToSay);
                await webhook.delete();
            })
        });
    }
}

exports.default = FakeUserCommand;