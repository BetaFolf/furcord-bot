/*
* Author: Beta Folf
* Project: Furcord/Bot
* File: src/Cogs/Information/Uptime.js
*/

// Imports
const { Command, Bot } = require("discordbull");
const { RichEmbed } =  require("discord.js");
const { Globals } = require("../../../Files/Globals");

// Command
class UptimeCommand extends Command {
    constructor() {
        super({
            NAME: "uptime",
            USAGE: "uptime",
            RUN: async (context) => {
                let embed = new RichEmbed();

                embed.setColor(Globals["EMBED_COLOR"]);
                embed.setTitle("Furcord Uptime");
                
                let totalSeconds = (Bot.DiscordClient.uptime / 1000);
                let days = Math.floor(totalSeconds / 86400);
                let hours = Math.floor(totalSeconds / 3600);
                totalSeconds %= 3600;
                let minutes = Math.floor(totalSeconds / 60);
                let seconds = Math.floor(totalSeconds % 60);
                
                embed.setDescription(`${days} days, ${hours} hours, ${minutes} minutes and ${seconds} seconds`);

                embed.setTimestamp();

                await context.channel.send(embed);
            }
        });
    }
}

exports.default = UptimeCommand;