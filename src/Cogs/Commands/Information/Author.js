/*
* Author: Beta Folf
* Project: Furcord/Bot
* File: src/Cogs/Commands/Information/Author.js
*/

// Imports
const { RichEmbed } = require("discord.js");
const { Command, Bot } = require("discordbull");
const { Globals } = require("../../../Files/Globals");

// Command
class AuthorCommand extends Command {
    constructor() {
        super({
            NAME: "author",
            USAGE: "author",
            RUN: async (context) => {
                let embed = new RichEmbed();

                embed.setColor(Globals["EMBED_COLOR"]);
                embed.setTitle("Furcord Author");
                
                let author = await Bot.DiscordClient.fetchUser(Bot.getOwners()[0])

                embed.setDescription(`${author.username}#${author.discriminator}`);
                embed.setThumbnail(author.displayAvatarURL);

                await context.channel.send(embed);
            }
        });
    }
}

exports.default = AuthorCommand;