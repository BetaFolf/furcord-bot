/*
* Author: Beta Folf
* Project: Furcord/Bot
* File: src/Cogs/Information/Help.js
*/

// Imports
const { RichEmbed } = require("discord.js");
const { Command } = require("discordbull");
const { Globals } = require("../../../Files/Globals");

// Command
class HelpCommand extends Command {
    constructor() {
        super({
            NAME: "help",
            USAGE: "help",
            NICKS: ["commands", "cmds"],
            RUN: async (context) => {
                let embed = new RichEmbed();

                embed.setColor(Globals["EMBED_COLOR"]);
                embed.setTitle("Furcord Help");
                
                embed.setDescription("Commands can be found online at https://www.furcord.net/commands");

                await context.react("✅");
                await context.author.send(embed);
            }
        });
    }
}

exports.default = HelpCommand;