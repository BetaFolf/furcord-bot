/*
* Author: Beta Folf
* Project: Furcord/Bot
* File: src/Cogs/Information/Website.js
*/

// Imports
const { Command } = require("discordbull");
const { RichEmbed } =  require("discord.js");
const { Globals } = require("../../../Files/Globals");

// Command
class WebsiteCommand extends Command {
    constructor() {
        super({
            NAME: "website",
            USAGE: "website",
            NICKS: ["web", "site"],
            RUN: async (context) => {
                let embed = new RichEmbed();

                embed.setColor(Globals["EMBED_COLOR"]);
                embed.setTitle("Furcord Website");
                
                embed.setDescription("Furcord website: https://www.furcord.net/");

                await context.react("✅");
                await context.author.send(embed);
            }
        });
    }
}

exports.default = WebsiteCommand;