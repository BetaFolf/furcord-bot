/*
* Author: Beta Folf
* Project: Furcord/Bot
* File: src/Cogs/Commands/information/Ping.js
*/

// Imports
const { Command, Bot } = require("discordbull");
const { RichEmbed } =  require("discord.js");
const { Globals } = require("../../../Files/Globals");

// Command
class PingCommand extends Command {
    constructor() {
        super({
            NAME: "ping",
            USAGE: "ping",
            RUN: async (context) => {
                let m = await context.channel.send("Ping?");

                let embed = new RichEmbed();

                embed.setColor(Globals["EMBED_COLOR"]);
                embed.setTitle("Pong!");
                
                embed.addField("Bot", `${m.createdTimestamp - context.createdTimestamp} ms`);
                embed.addField("API", `${Math.round(Bot.DiscordClient.ping)} ms`);

                embed.setTimestamp();

                await m.edit(embed);
            }
        });
    }
}

exports.default = PingCommand;