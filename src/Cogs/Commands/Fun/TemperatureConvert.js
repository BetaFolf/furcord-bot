/*
* Author: Beta Folf
* Project: Furcord/Bot
* File: src/Cogs/Commands/Fun/TemperatureConvert.js
*/

// Imports
const { Command } = require("discordbull");

// Command
class TemperatureConvert extends Command {
    constructor() {
        super({
            NAME: "temperatureconvert",
            USAGE: "temperatureconvert",
            NICKS: ["temp", "tempconvert", "tc"],
            ARGS: [
                {
                    NAME: "from",
                    REQUIRED: true
                },
                {
                    NAME: "number",
                    REQUIRED: true
                }
            ],
            RUN: async (context, args) => {
                let from = args[0].toUpperCase();
                let number = args[1];

                let answer = 0;

                if (isNaN(number)) {
                    return await context.channel.send("You need to pass a number to convert!");
                }

                if (from === "C" || from === "CELCIUS") {
                    answer = (number * 9/5) + 32;
                    return await context.channel.send(`${number} degrees Celcius is ${Math.floor(answer)} degrees Fahrenheit.`);
                } else if (from === "F" || from === "FAHRENHEIT") {
                    answer = (number - 32) * 5/9;
                    return await context.channel.send(`${number} degrees Fahrenheit is ${Math.floor(answer)} degrees Celcius.`);
                } else {
                    return await context.channel.send("You can only convert from Celcius and Fahrenheit.");
                }
            }
        });
    }
}

exports.default = TemperatureConvert;