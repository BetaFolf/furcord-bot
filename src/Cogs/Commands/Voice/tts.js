/*
* Author: Beta Folf
* Project: Furcord/Bot
* File: src/Cogs/Commands/Voice/tts.js
*/

// Imports
const { Bot, Command } = require("discordbull");
const tts = require("google-tts-api");
const fs = require("fs");

// Command
class TTSCommand extends Command {
    constructor() {
        super({
            NAME: "tts",
            USAGE: "tts",
            ARGS: [
                {
                    NAME: "text",
                    REQUIRED: true
                }
            ],
            GUILD_ONLY: true,
            RUN: async (context, args) => {
                let text = args.join(" ") + "a"

                if (context.member.voiceChannel === null || context.member.voiceChannel === undefined) {
                    return await context.channel.send("You must be in a voice channel to use this command!");
                } else {
                    if (args.length > 200) {
                        return await context.channel.send("You cannot use text to speech messages over 200 characters!");
                    } else {
                        context.member.voiceChannel.join().then(async connection => {
                            tts(text, "en", 1).then(async url => {
                                const broadcast = Bot.DiscordClient.createVoiceBroadcast().playArbitraryInput(url);
                                await connection.playBroadcast(broadcast);
                                await connection.disconnect();
                            }).catch(async error => {
                                return await context.channel.send("Could not create text to speech!");
                            });

                            await context.react("✅");
                        }).catch(async error => {
                            console.log(error)
                            return await context.channel.send("Could not join voice channel!");
                        });
                    }
                }
            }
        });
    }   
}

exports.default = TTSCommand;