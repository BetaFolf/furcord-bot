/*
* Author: Beta Folf
* Project: Furcord/Bot
* File: src/Cogs/Commands/Moderation/Approve.js
*/

// Imports
const { Bot, Command, PermissionFlags } =  require("discordbull");

// Command
class ApproveCommand extends Command {
    constructor() {
        super({
            NAME: "approve",
            USAGE: "approve",
            ARGS: [
                {
                    NAME: "users",
                    REQUIRED: true
                }
            ],
            PERMISSIONS_REQUIRED: [PermissionFlags.MANAGE_ROLES],
            RUN: async (context, args) => {
                let user = args[0].replace("<", "").replace(">", "").replace("@", "").replace("!", "");

                user = await Bot.DiscordClient.fetchUser(user)

                let member = await context.guild.fetchMember(user)
                await member.removeRole("571732205903020042");
                await member.addRole("571730226418286613");

                await context.channel.send("Updated roles!");
            }
        });
    }
}

exports.default = ApproveCommand;