/*
* Author: Beta Folf
* Project: Furcord/Bot
* File: src/Cogs/Events/Ready.js
*/

// Import
const { Event, Log, Bot } = require("discordbull");

// Class
class ReadyEvent extends Event {
    constructor() {
        super();
        this.ready(async () => {
            await Bot.DiscordClient.user.setActivity("furries!", { type: "WATCHING" });
            Log("Ready!");
        });
    }
}

exports.default = ReadyEvent;