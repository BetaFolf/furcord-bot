/*
* Author: Beta Folf
* Project: Furcord/Bot
* File: src/Cogs/Events/Error.js
*/

// Imports
const { Bot, Event } = require("discordbull");

// Command
class ErrorEvent extends Event {
    constructor() {
        super();
        this.error(error => {
            console.log(error)
        });
    }
}

exports.default = ErrorEvent;