/*
* Author: Beta Folf
* Project: Furcord/Bot
* File: src/Cogs/Events/UserJoin.js
*/

// Imports
const { Bot, Event } = require("discordbull");

// Event
const hiddenPhrases = [
    "milkshake",
    "lawsuits",
    "apple",
    "buttercup",
    "cheese",
    "water",
    "air",
    "o2",
    "keyboard",
    "beer",
    "mouse",
    "monitor",
    "random"
]

class UserJoinEvent extends Event {
    constructor() {
        super();
        this.guildMemberAdd(async member => {
            try {
                let difference = (new Date().getTime() - member.user.createdAt) / 1000;

                if (difference < 604800) {
                    await member.send("Your account has to be at least a week old to join Furcord.");
                    return await member.kick();
                }

                let hiddenPhrase = hiddenPhrases[Math.floor(Math.random() * hiddenPhrases.length)];

                let phrase = `Hidden phrase to gain entry is ${hiddenPhrase}`

                var baseRules =
`
<@${member.id}>

Welcome to Furcord!
~~-----------------------~~
                
In order to gain access to the server you must read these rules and repeat the hidden phrase in the verify channel.

1.) Don't create drama intentionality.
    - We don't like drama. No one does. If you go out of your way to cause drama you will face punishment.
2.) Staff have the final say. Staff are the people with the roles of \`Owner\`, \`Co-Owner\`, \`Administrator\`, \`Moderator\`, or \`Trial Mod\`.
    - Don't argue with them. They will assess a given situation and determine what is the best course of action. If you have an issue with a specific staff member, talk to the Owner.
3.) No hatred towards other groups.
    - This includes use of the n word and other derogatory terms. It's simple. We're all people and are all equal no matter race, age, sexual orientation, or any other things about a person.
4.) No over excessive profanity
    - We don't mind swearing, but if you drop an f bomb in every single message you may face punishment. 
5.) No trying to get someone's personal information without their consent or sharing any personal information of someone in the server without their consent.
    - Pretty self explanatory. Sending an IP address grabber will result in an immediate ban.
6.) Don't ping the owner or staff unnecessarily.
7.) Don't spam. Spam counts as more than three of the same or tiny variation of a message in a row.
8.) Use designated channels.
9.) Don't joke about mental illness or suicide.
10.) Don't advertise other servers unless partnered with us.
11.) Don't talk about gross or disturbing topics. I.E. Zoophillia. This includes illegal topics.
12.) A furry Discord avatar that can be found on the first few pages of Google will get you kicked. This is due to this being a tactic used by raiders.
13.) We're here to have fun and socialize. Use common sense.
`
                baseRules = baseRules.split(" ")

                let random = Math.floor(Math.random() * baseRules.length)

                for (let x = 0; x < baseRules.length; x++) {
                    if (x === random) {
                        baseRules[x] = ` ${phrase} `
                    }
                }

                baseRules = baseRules.join(" ")

                await member.send(baseRules);
                await member.addRole("571732205903020042");

                let filter = m => m.content.includes("");
                let collector = await Bot.DiscordClient.channels.get("571731443026493481").createMessageCollector(filter);
                collector.on("collect", async m => {
                    if (m.author.id.toString() === member.user.id.toString()) {
                        if (m.content.toUpperCase() === hiddenPhrase.toUpperCase()) {
                            await member.removeRole("571732205903020042")
                            await member.addRole("571730226418286613")
                        }
                    }
                });
            } catch (error) {
                console.log(error);
            }
        });
    }
}

exports.default = UserJoinEvent;