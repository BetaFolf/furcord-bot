/*
* Author: Beta Folf
* Project: Furcord/Bot
* File: src/Files/Globals.js
*/

// Exports
const Globals = {
    "EMBED_COLOR": "0x007289DA"
}

module.exports = { Globals };