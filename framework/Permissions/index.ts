/*
* Author: Beta Folf
* Project: Furcord/Bot
* File: framework/Permissions/index.ts
*/

// Imports
import { Permissions } from "discord.js";

const PermissionFlags = Permissions.FLAGS;

export default PermissionFlags;