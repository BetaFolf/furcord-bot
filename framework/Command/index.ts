/*
* Author: Beta Folf
* Project: Furcord/Bot
* File: framework/Command/index.ts
*/

// Imports
import Parameter from "../Interfaces/Parameter";
import CommandInterface from "../Interfaces/Command";
import PermissionFlags from "../Permissions";
import Globals from "../Globals"

// Class
export default class Command {
    protected NAME: string;
    protected USAGE: string;
    protected NICKS: Array<string>;
    protected ARGS: Array<Parameter>;
    protected NSFW: Boolean;
    protected OWNER_ONLY: Boolean;
    protected PERMISSIONS_REQUIRED: Array<any>;
    protected RUN: any;
    protected GUILD_ONLY: Boolean;

    constructor(params: CommandInterface) {
        this.NAME = params["NAME"];
        this.USAGE = params["USAGE"];
        this.NICKS = params["NICKS"] || [];
        this.ARGS = params["ARGS"] || [];
        this.NSFW = params["NSFW"] || false;
        this.OWNER_ONLY = params["OWNER_ONLY"] || false;
        this.PERMISSIONS_REQUIRED = params["PERMISSIONS_REQUIRED"];
        this.RUN = params["RUN"];
        this.GUILD_ONLY = params["GUILD_ONLY"] || false;

        let command = {
            "name": this.NAME,
            "usage": this.USAGE,
            "nicks": this.NICKS,
            "args": this.ARGS,
            "nsfw": this.NSFW,
            "owner_only": this.OWNER_ONLY,
            "permissions": this.PERMISSIONS_REQUIRED,
            "run": this.RUN,
            "guild_only": this.GUILD_ONLY
        }
        
        Globals["COMMANDS"].push(command);
    }
}