/*
* Author: Beta Folf
* Project: Furcord/Bot
* File: framework/Bot/index.ts
*/

// Imports
import { Client } from "discord.js";
import BotInformation from "../Interfaces/BotInformation";
import CommandListener from "../CommandListener";
import * as fs from "fs";
import * as path from "path";

// Bot
class Bot {
    private TOKEN: string;
    private static OWNER_IDS: Array<string>;
    private static EMBED_COLOR: string;
    private COGS_LOCATION: string;

    private static DiscordClient : Client = new Client();

    constructor(params: BotInformation) {
        this.TOKEN = params.TOKEN;
        Bot.OWNER_IDS = params.OWNER_IDS;
        Bot.EMBED_COLOR = params.EMBED_COLOR;
        this.COGS_LOCATION = params.COGS_LOCATION;
    
        Bot.LoadCogs(this.COGS_LOCATION);

        new CommandListener(Bot.DiscordClient);

        Bot.DiscordClient.login(this.TOKEN);
    }

    public static LoadCogs(cogsLocation: string) {
        fs.readdirSync(cogsLocation).forEach(file => {
            let fileLocation = path.join(cogsLocation, file);
            if (fs.statSync(fileLocation).isDirectory()) {
                Bot.LoadCogs(fileLocation);
            } else {
                try {
                    let f = require(fileLocation).default;
                    new f();
                } catch(e) {
                    console.log(`Could not load cog '${file}'. \nError: '${e}'`);
                }        
            }
        });
    }

    public static getOwners(): Array<string> { return Bot.OWNER_IDS; }
    public static getEmbedColor(): string { return Bot.EMBED_COLOR; }
    public static getClient(): Client { return Bot.DiscordClient; }
}

export { Bot };