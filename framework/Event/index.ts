/*
* Author: Beta Folf
* Project: Furcord/Bot
* File: framework/Event/index.ts
*/

// Imports
import { Bot } from "../Bot";

const Client = Bot.getClient();

// Class
export default class Event {
    protected channelCreate(run: any) { Client.on("channelCreate", run) }
    protected channelDelete(run: any) { Client.on("channelDelete", run) }
    protected channelPinsUpdate(run: any) { Client.on("channelPinsUpdate", run) }
    protected clientUserGuildSettingsUpdate(run: any) { Client.on("channelCreate", run) }
    protected clientUserSettingsUpdate(run: any) { Client.on("clientUserSettingsUpdate", run) }
    protected debug(run: any) { Client.on("debug", run) }
    protected disconnect(run: any) { Client.on("disconnect", run) }
    protected emojiCreate(run: any) { Client.on("emojiCreate", run) }
    protected emojiDelete(run: any) { Client.on("emojiDelete", run) }
    protected emojiUpdate(run: any) { Client.on("emojiUpdate", run) }
    protected error(run: any) { Client.on("error", run) }
    protected guildBanAdd(run: any) { Client.on("guildBanAdd", run) }
    protected guildBanRemove(run: any) { Client.on("guildBanRemove", run) }
    protected guildCreate(run: any) { Client.on("guildCreate", run) }
    protected guildDelete(run: any) { Client.on("guildDelete", run) }
    protected guildMemberAdd(run: any) { Client.on("guildMemberAdd", run) }
    protected guildMemberRemove(run: any) { Client.on("guildMemberRemove", run) }
    protected guildMembersChunk(run: any) { Client.on("guildMembersChunk", run) }
    protected guildMemberSpeaking(run: any) { Client.on("guildMemberSpeaking", run) }
    protected guildMemberUpdate(run: any) { Client.on("guildMemberUpdate", run) }
    protected guildUnavailable(run: any) { Client.on("guildUnavailable", run) }
    protected guildUpdate(run: any) { Client.on("guildUpdate", run) }
    protected message(run: any) { Client.on("message", run) }
    protected messageDelete(run: any) { Client.on("messageDelete", run) }
    protected messageDeleteBulk(run: any) { Client.on("messageDeleteBulk", run) }
    protected messageReactionAdd(run: any) { Client.on("messageReactionAdd", run) }
    protected messageReactionRemove(run: any) { Client.on("messageReactionRemove", run) }
    protected messageReactionRemoveAll(run: any) { Client.on("messageReactionRemoveAll", run) }
    protected messageUpdate(run: any) { Client.on("messageUpdate", run) }
    protected presenceUpdate(run: any) { Client.on("presenceUpdate", run) }
    protected rateLimit(run: any) { Client.on("rateLimit", run) }
    protected ready(run: any) { Client.on("ready", run) }
    protected reconnecting(run: any) { Client.on("reconnecting", run) }
    protected resume(run: any) { Client.on("resume", run) }
    protected roleCreate(run: any) { Client.on("roleCreate", run) }
    protected roleDelete(run: any) { Client.on("roleDelete", run) }
    protected roleUpdate(run: any) { Client.on("roleUpdate", run) }
    protected typingStart(run: any) { Client.on("typingStart", run) }
    protected typingStop(run: any) { Client.on("typingStop", run) }
    protected userNoteUpdate(run: any) { Client.on("userNoteUpdate", run) }
    protected userUpdate(run: any) { Client.on("userUpdate", run) }
    protected voiceStateUpdate(run: any) { Client.on("voiceStateUpdate", run) }
    protected warn(run: any) { Client.on("warn", run) }
}