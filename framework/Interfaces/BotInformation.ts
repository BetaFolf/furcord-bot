/*
* Author: Beta Folf
* Project: Furcord/Bot
* File: framework/Interfaces/BotInformation.ts
*/

export default interface BotInformation {
    TOKEN: string,
    OWNER_IDS: Array<string>,
    EMBED_COLOR: string,
    COGS_LOCATION: string
}