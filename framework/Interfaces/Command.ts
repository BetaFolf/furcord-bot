/*
* Author: Beta Folf
* Project: Furcord/Bot
* File: framework/interfaces/Command.ts
*/

// Import
import Parameter from "./Parameter";
import Permissions from "../Permissions";

// Class
export default interface CommandInterface {
    NAME: string,
    USAGE: string
    NICKS: Array<string>,
    ARGS: Array<Parameter>,
    NSFW: Boolean,
    OWNER_ONLY: Boolean,
    PERMISSIONS_REQUIRED: Array<any>,
    RUN: any,
    GUILD_ONLY: Boolean
}