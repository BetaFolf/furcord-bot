/*
* Author: Beta Folf
* Project: Furcord/Bot
* File: framework/Interfaces/Parameter.ts
*/

export default interface Parameter {
    NAME: string,
    REQUIRED: Boolean
}