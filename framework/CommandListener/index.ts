/*
* Author: Beta Folf
* Project: Furcord/Bot
* File: framework/Bot/index.ts
*/

// Imports
import Globals from "../Globals";
import { Bot } from "../Bot";
import { TextChannel } from "discord.js";
import { Client } from "discord.js";

// Command listener
class CommandListener {
    constructor(client: Client) {
        let prefix = "+";

        client.on("message", async initial => {
            if (initial.author.bot) {
                return
            } else {
                var run = false;
                let command = initial.content.replace(/ +(?= )/g, "").split(" ")[0].toUpperCase()

                let isCommand = false;
                let cmd = null;

                let first = command.split("");

                if (first[0] === prefix) {
                    isCommand = true;
                }

                command = command.replace(prefix, "");

                var args : any = [];

                if (isCommand) {
                    for (let x = 0; x < Globals["COMMANDS"].length; x++) {
                        if (Globals["COMMANDS"][x]["usage"].toUpperCase() === command) {
                            cmd = Globals["COMMANDS"][x]
                        } else {
                            for (let y = 0; y < Globals["COMMANDS"][x]["nicks"].length; y++) {
                                if (Globals["COMMANDS"][x]["nicks"][y].toUpperCase() === command) {
                                    cmd = Globals["COMMANDS"][x]
                                }
                            }
                        }
                    }

                    if (cmd != null) {
                        if (cmd["owner_only"]) {
                            if (Bot.getOwners().includes(initial.author.id) === false) {
                                return await initial.channel.send("Only the bot's owner can use this command!");
                            }
                        }
                        if (cmd["nsfw"]) {
                            if (initial.channel.type === "text") {
                                let channel = <TextChannel> initial.channel;
                                if (channel.nsfw === false) {
                                    return await initial.channel.send("This command can only be used in NSFW marked channels!");
                                }
                            }
                        }
                        if (cmd["guild_only"]) {
                            let channel = <TextChannel> initial.channel;
                            if (channel.guild === null) {
                                return await initial.channel.send("This command can only be used in guilds!");
                            }
                        }
                        if (cmd["permissions"] != null) {
                            const perms = cmd["permissions"];
                            if (initial.channel.type === "text") {
                                let user = await initial.guild.fetchMember(initial.author.id)
                                if (await user.hasPermission(perms) === false) {
                                    return await initial.channel.send("You don't have permission to do this command!");
                                }
                            }
                        }
                        if (cmd["args"] != []) {
                            let argsGiven = initial.content.replace(/ +(?= )/g, "").split(" ")

                            argsGiven.shift();

                            let requiredCount = 0;

                            for (let x = 0; x < cmd["args"].length; x++) {
                                if (cmd["args"][x]["REQUIRED"]) {
                                    requiredCount = requiredCount + 1;
                                }
                            }

                            for (let x = 0; x < argsGiven.length; x++) {
                                args.push(argsGiven[x])
                            }

                            if (argsGiven.length < requiredCount) {
                                return await initial.channel.send("You didn't send enough parameters!");
                            }
                            
                            return cmd["run"](initial, args);
                        }
                        return cmd["run"](initial);
                    }
                }
            }
        })
    }
}

export default CommandListener;