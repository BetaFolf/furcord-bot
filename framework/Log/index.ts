/*
* Author: Beta Folf
* Project: Furcord/Bot
* File: framework/Log.index.ts
*/

// Imports
import chalk from "chalk";
import * as moment from "moment";

// Export
const Log = (content: string) => {
    return console.log(`${chalk.blue("Furcord")} | ${chalk.red(moment().format('MMMM Do YYYY, h:mm:ss a'))} > ${chalk.white(content)}`);
};

export default Log;